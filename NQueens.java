public class NQueens {
    public List<List<String>> solveNQueens(int n) {
        List<List<String>> rst = new ArrayList<List<String>>();
        if (n <= 0) return rst;
        search(n, new ArrayList<Integer>(), rst);
        return rst;
    }

    private void search(int n, ArrayList<Integer> col, List<List<String>> rst) {
        if (col.size() == n) {
            rst.add(drawChessBoard(col));
            return;
        }

        for (int i = 0; i < n; i++) {
            if (!isValid(col, i)) continue;
            col.add(i);
            search(n, col, rst);
            col.remove(col.size() - 1);
        }
    }

    private boolean isValid(ArrayList<Integer> col, int next) {
        int row = col.size();
        for (int i = 0; i < row; i++) {
            if (next == col.get(i)) return false;
            if (i - row == col.get(i) - next) return false;
            if (i - row == next - col.get(i)) return false;
        }
        return true;
    }

    private ArrayList<String> drawChessBoard(ArrayList<Integer> col) {
        ArrayList<String> chessBoard = new ArrayList<String>();

        for (int i = 0; i < col.size(); i++) {
            String row = "";
            for (int j = 0; j < col.size(); j++) {
                if (col.get(j) == i) {
                    row += "Q";
                } else {
                    row += ".";
                }
            }
            chessBoard.add(row);
        }
        return chessBoard;
    }
}