// Version 1: Ask what is the highest stack the boxes can make, do not need to print boxes, DP solution

import java.util.*;
public class Solution{
    public int createStackDP (Box[] boxes) {
        int max = 0;
        int[] dp = new int[boxes.length];

        Arrays.sort(boxes, (Box a, Box b)->((a.w == b.w) ? (a.d - b.d):(a.w - b.w)));
        for (int j = 0; j < boxes.length; j++) {
            dp[j] = boxes[j].h;
            for (int i = 0; i < j; i++) {
                if (boxes[j].canBeUnder(boxes[i])) {
                    dp[j] = Math.max(dp[i] + boxes[j].h, dp[j]);
                    max = Math.max(max, dp[j]);
                }
            }
        }
        return max;
    }

    private int getStackHeight(List<Box> boxes) {
        int sum = 0;
        for (Box b: boxes) {
            sum += b.h;
        }
        return sum;
    }
    public static void main (String[] args) {
        Solution sln = new Solution();
        Box[] boxes = {new Box(3, 4, 1), new Box(8, 6, 2), new Box(7, 8, 3)};
        System.out.println(sln.createStackDP(boxes));
    }
}
class Box {
    int w;
    int d;
    int h;
    public Box (int width, int depth, int height) {
        this.w = width;
        this.d = depth;
        this.h = height;
    }

    public boolean canBeUnder(Box b) {
        return (w > b.w && h > b.h && d > b.d);
    }

    public boolean canBeAbove(Box b) {
        if (b == null) return true;
        return (w < b.w && h < b.h && d < b.d);
    }

    public String toString() {
        return "Box(" + w + ", " + d + ", " + h + ")";
    }
}



// V2:  Need to find the highest stack, and print out the boxes

import java.util.*;
public class Solution{
    public List<Box> createStackDP (Box[] boxes) {
        int max = 0;
        List<Box> max_stack = null;

        int[] dp = new int[boxes.length];
        Map<Box, List<Box>> map = new HashMap<>();

        Arrays.sort(boxes, (Box a, Box b)->((a.w == b.w) ? (a.d - b.d):(a.w - b.w)));
        for (int j = 0; j < boxes.length; j++) {
            dp[j] = boxes[j].h;
            List<Box> curr = new ArrayList<>();
            curr.add(boxes[j]);
            map.put(boxes[j], curr);

            for (int i = 0; i < j; i++) {
                if (boxes[j].canBeUnder(boxes[i])) {
                    if (dp[i] + boxes[j].h > dp[j]) {
                        dp[j] = dp[i] + boxes[j].h;
                        List<Box> stack = new ArrayList<Box>(map.get(boxes[i]));
                        stack.add(boxes[j]);
                        map.put(boxes[j], stack);
                    }

                    if (dp[j] > max) {
                        max = dp[j];
                        max_stack = new ArrayList<>(map.get(boxes[j]));
                    }
                }
            }
        }
        return max_stack;
    }

    private int getStackHeight(List<Box> boxes) {
        int sum = 0;
        for (Box b: boxes) {
            sum += b.h;
        }
        return sum;
    }
    public static void main (String[] args) {
        Solution sln = new Solution();
        Box[] boxes = {new Box(3, 4, 1), new Box(8, 6, 2), new Box(7, 8, 3)};
        List<Box> rst = sln.createStackDP(boxes);
        for (Box b: rst) {
            System.out.println(b.toString());
        }
    }
}
class Box {
    int w;
    int d;
    int h;
    public Box (int width, int depth, int height) {
        this.w = width;
        this.d = depth;
        this.h = height;
    }

    public boolean canBeUnder(Box b) {
        return (w > b.w && h > b.h && d > b.d);
    }

    public boolean canBeAbove(Box b) {
        if (b == null) return true;
        return (w < b.w && h < b.h && d < b.d);
    }

    public String toString() {
        return "Box(" + w + ", " + d + ", " + h + ")";
    }
}

// V3: More general: can be used for multiple instances of Boxes, can rotate
// Find the max height can reach with box[i] being bottom
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 */

public class StackOfBoxes {
    static class Box {
        int width;
        int height;
        int depth;
        public Box(int w, int h, int d) {
            this.width = w;
            this.height = h;
            this.depth = d;
        }

        public boolean canBeUnder(Box b) {
            if (width > b.width && height > b.height && depth > b.depth) {
                return true;
            } else {
                return false;
            }
        }

        public boolean canBeAbove(Box b) {
            if (b == null) return true;
            if (width < b.width && height < b.height && depth < b.depth) {
                return true;
            } else {
                return false;
            }
        }

        public String toString() {
            return "Box(" + width + ", " + height + ", " + depth + ")";
        }
    }

    public ArrayList<Box> createStackDP(Box[] boxes, Box bottom, HashMap<Box, ArrayList<Box>> map) {
        if (bottom != null && map.containsKey(bottom)) {
            return (new ArrayList<Box>(map.get(bottom)));
        }

        int max_height = 0;
        ArrayList<Box> max_stack = null;
        for (int i = 0; i < boxes.length; i++) {
            if (boxes[i].canBeAbove(bottom)) {
                ArrayList<Box> new_stack = createStackDP(boxes, boxes[i], map);
                int new_height = stackHeight(new_stack);
                if (new_height > max_height) {
                    max_height = new_height;
                    max_stack = new_stack;
                }
            }
        }

        if (max_stack == null) {
            max_stack = new ArrayList<Box>();
        }
        if (bottom != null) {
            max_stack.add(0, bottom);
        }
        map.put(bottom, max_stack);
        return max_stack;
    }

    private int stackHeight(ArrayList<Box> boxes) {
        int sum = 0;
        for (Box b: boxes) {
            sum += b.height;
        }
        return sum;
    }
    public static void main(String[] args) {
        StackOfBoxes obj = new StackOfBoxes();
        Box[] boxes = {new Box(3, 4, 1), new Box(8, 6, 2), new Box(7, 8, 3)};

        ArrayList<Box> stack = obj.createStackDP(boxes, null, new HashMap<Box, ArrayList<Box>>());
        //ArrayList<Box> stack = createStackR(boxes, null);
        for (int i = stack.size() - 1; i >= 0; i--) {
            Box b = stack.get(i);
            System.out.println(b.toString());
        }
    }
}
