class SeiveOfEratosthenes {
    public int countPrimes(int n) {
        if (n <= 2) return 0;
        int[] primes = new int[n];
        Arrays.fill(primes, 1);
        primes[0] = 0;
        primes[1] = 0;
        int count = n - 2;
        int rt = (int) Math.sqrt(n);
        for (int i = 2; i <= rt; i++) {
            if (primes[i] == 1) {
                for (int j = i * i; j < n; j += i) {
                    if (primes[j] == 1) {
                        primes[j] = 0;
                        count--;
                    }
                }
            }
        }
        return count;
    }
}