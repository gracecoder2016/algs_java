import java.util.concurrent.locks.*;

public class PhilosopherQuestion {
    public static int size = 3;

    public static int leftOf(int i) {
        return i;
    }

    public static int rightOf(int i) {
        return (i + 1) % size;
    }

    public static void main(String[] args) {
        Chopstick[] chopsticks = new Chopstick[size + 1];
        for (int i = 0; i < size + 1; i++) {
            chopsticks[i] = new Chopstick();
        }

        Philosopher[] philosophers = new Philosopher[size];
        for (int i = 0; i < size; i++) {
            Chopstick left = chopsticks[leftOf(i)];
            Chopstick right = chopsticks[rightOf(i)];
            philosophers[i] = new Philosopher(i, left, right);
        }
        for (int i = 0; i < size; i++) {
            philosophers[i].start();
        }
    }
}

class Philosopher extends Thread {
    private final int minPause = 998;
    private final int maxPause = 1002;
    private int bites = 5;

    private Chopstick left;
    private Chopstick right;
    private int index;

    public Philosopher(int i, Chopstick left, Chopstick right) {
        index = i;
        this.left = left;
        this.right = right;
    }

    public void pause() {
        try {
            int pause = AssortedMethods.randomIntInRange(minPause, maxPause);
            System.out.println("Philosopher " + index + " will wait for "+ pause+ " ms.");
            Thread.sleep(pause);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public boolean pickUp() {
        pause();
        if (!left.pickUp()) return false;
        pause();
        if (!right.pickUp()) return false;
        pause();
        return true;
    }

    public void putDown() {
        left.putDown();
        right.putDown();
    }

    public void chew() {
        System.out.println("Philosopher " + index + ": eating");
        pause();
    }

    public boolean eat() {
        if (pickUp()) {
            System.out.println("Philosopher " + index + ": start eating");
            chew();
            putDown();
            System.out.println("Philosopher " + index + ": done eating");
            return true;
        } else {
            System.out.println("Philosopher " + index + ": gave up on eating");
            return false;
        }
    }

    public void run() {
        int i = 0;
        while (i < bites) {
            if (eat()) {
                i++;
            }
        }
    }
}

class Chopstick {
    private Lock lock;

    public Chopstick() {
        lock = new ReentrantLock();
    }

    public boolean pickUp() {
        return lock.tryLock();
    }

    public void putDown() {
        lock.unlock();
    }
}