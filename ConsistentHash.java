import java.util.*;

public class ConsistentHash {
    private TreeMap<Integer, Integer> tm = new TreeMap<>();
    private int[] nums;
    private int size = 0;
    private int k;

    public ConsistentHash(int n, int k) {
        this.nums = new int[n];
        for (int i = 0; i < n; i++) {
            this.nums[i] = i;
        }

        Random rdm = new Random();
        for (int i = 0; i < n; i++) {
            int j = rdm.nextInt(i + 1);
            int t = nums[i];
            nums[i] = nums[j];
            nums[j] = t;
        }
        this.k = k;
    }

    public static ConsistentHash create (int n, int k) {
        return new ConsistentHash(n, k);
    }

    public List<Integer> addMachine(int machine_id) {
        List<Integer> ids = new ArrayList<>();
        for (int i = 0; i < this.k; i++) {
            int id = this.nums[size++ % this.nums.length];
            ids.add(id);
            this.tm.put(id, machine_id);  // Map<virtual node, machine_id>
        }
        return ids;
    }

    public int getMachineIdByHashCode (int hashcode) {
        if (tm.isEmpty()) return 0;
        Integer ceiling = tm.ceilingKey(hashcode);
        if (ceiling != null) return tm.get(ceiling); // find hashcode belongs to which machine
        return tm.get(tm.firstKey());
    }

    public static void main(String[] args) {
        ConsistentHash obj = new ConsistentHash(100, 3);
        obj.addMachine(11);
        System.out.println(obj.getMachineIdByHashCode(4));
        obj.addMachine(22);
        System.out.println(obj.getMachineIdByHashCode(4));
        System.out.println(obj.getMachineIdByHashCode(50));
    }
}