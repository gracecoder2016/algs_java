V1: Easy to understand
class LRUCache {
    private Map<Integer, Integer> map;
    private int capacity;
    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new LinkedHashMap<>(capacity);
    }

    public int get(int key) {
        if (!map.containsKey(key)) return -1;
        int val = map.get(key);
        map.remove(key);
        map.put(key, val);
        return val;
    }

    public void put(int key, int value) {
        if (map.size() == capacity ) {
            if (!map.containsKey(key)) {
                map.remove(map.entrySet().iterator().next().getKey());
            } else {
                map.remove(key);
            }
        }
        map.remove(key);
        map.put(key, value);
    }
}


// V2: use buildingIn function
public class LRUCache {
    private Map<Integer, Integer> map;
    private int capacity;
    public LRUCache(int capacity) {
        this.capacity = capacity;
        // define inner function to remove eldest entry when exceeds capacity
        map = new LinkedHashMap<Integer, Integer>(capacity, 1f, true) {
            protected boolean removeEldestEntry(Map.Entry eldest) { 
                return size() > capacity;
            }
        };
    }

    public int get(int key) {
        Integer val = map.get(key);
        if (val == null) return -1;
        map.remove(key);
        map.put(key, val);
        return val;
    }

    public void put (int key, int value) {
        map.put(key, value);
    }
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */
 
 V3: starting from scatch 
// get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
// put(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the //least recently used item before inserting a new item.
import java.util.*;
class LRUCache {
    private Map<Integer, DLNode> map;
    DLNode head;
    DLNode tail;
    private int capacity;
    private int count;

    public LRUCache(int capacity) {
        // if (capacity <= 0) throw new IllegalAccessException("Needs positive capcity");
        this.capacity = capacity;
        this.count = 0;
        map = new HashMap<>();
        head = new DLNode();
        tail = new DLNode();
        head.next = tail;
        tail.prev = head;
    }

    private void addNode(DLNode node) {
        // add new node after head
        node.next = head.next;
        node.prev = head;
        head.next.prev = node;
        head.next = node;
    }

    private void removeNode (DLNode node) {
        DLNode prev = node.prev;
        DLNode next = node.next;
        prev.next = next;
        next.prev = prev;
    }

    private void moveToHead (DLNode node) {
        removeNode(node);
        addNode(node);
    }

    private DLNode pop() {
        DLNode rst = tail.prev;
        removeNode(tail.prev);
        return rst;
    }

    public int get(int key) {
        DLNode node = map.get(key);
        if (node == null) return -1;
        moveToHead(node);
        return node.val;
    }

    public void put(int key, int value) {
        DLNode node = map.get(key);
        if (node == null) {
            count++;
            if (count > capacity) {
                DLNode old = pop();
                map.remove(old.key);
                count--;
            }
            node = new DLNode(key, value);
            map.put(key, node);
            addNode(node);
        } else {
            node.val = value;
            moveToHead(node);
        }
    }
    public static void main (String[] args) {
        // LRU cache = new LRU(0);
        // LRU cache = new LRU(-1);
        LRU cache = new LRU(4);
        cache.put(0, 0);
        System.out.println(cache.get(0)); // 0
        System.out.println(cache.get(-1)); // -1
        cache.put(1, 1);
        cache.put(2, 2);
        cache.put(3, 3);
        cache.put(4, 4);
        System.out.println(cache.get(0)); // -1;
        System.out.println(cache.get(1)); // 1
        cache.put(5, 5);
        System.out.println(cache.get(1)); // 1
        System.out.println(cache.get(2)); // -1
        //cache.get("0"); // -1
        System.out.println(cache.get(4));
        cache.put(6, 6);
        System.out.println(cache.get(4)); // 4
        cache.put(5, 55);
        System.out.println(cache.get(4)); // 4
        System.out.println(cache.get(5)); // 55
    }
}

class DLNode {
    int key;
    int val;
    DLNode prev;
    DLNode next;
    DLNode(){}
    DLNode (int key, int value) {
        this.key = key;
        this.val = value;
        this.prev = null;
        this.next = null;
    }
}