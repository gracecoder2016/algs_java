public class BasicCalculator {
    public int calculate(String s) {
        return solveRPN(getRPN(s));
    }
    private String getRPN(String s) {
        Stack<Character> stack = new Stack<>();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == ' ') continue;
            if (Character.isDigit(c)) {
                StringBuilder num = new StringBuilder();
                while (i < s.length() && Character.isDigit(s.charAt(i))) {
                    num.append(s.charAt(i++));
                }
                i--;
                sb.append(Integer.parseInt(num.toString()));
            } else {
                sb.append(' ');
                if (c == '(') {
                    stack.push(c);
                } else if (c == ')') {
                    while (!stack.isEmpty() && stack.peek() != '(') {
                        sb.append(stack.pop());
                        sb.append(' ');
                    }
                    stack.pop();
                } else {
                    while (!stack.isEmpty() && getPrecedence(c) <= getPrecedence(stack.peek())) {
                        sb.append(stack.pop());
                        sb.append(' ');
                    }
                    stack.push(c);
                }
            }
        }
        while (!stack.isEmpty()) {
            sb.append(' ');
            sb.append(stack.pop());
        }
        return sb.toString();
    }
    private int solveRPN(String s) {
        String[] strs = s.split(" ");
        Stack<Integer> stack = new Stack<>();
        for (String str: strs) {
            if (str.equals("")) continue;
            if ("+-*/".contains(str)) {
                int num2 = stack.pop();
                int num1 = stack.pop();
                if (str.equals("+")) stack.push(num1 + num2);
                if (str.equals("-")) stack.push(num1 - num2);
                if (str.equals("*")) stack.push(num1 * num2);
                if (str.equals("/")) stack.push(num1 / num2);
            } else {
                stack.push(Integer.parseInt(str));
            }
        }
        return stack.pop();
    }
    private int getPrecedence(char c) {
        if (c == '*' || c == '/') return 3;
        if (c == '+' || c == '-') return 2;
        return 0;
    }
}

// V3: general
public class BasicCalculator {
    public int calculate(String s) {
        Stack<Character> ops = new Stack<>();
        Stack<Integer> vals = new Stack<>();
        char[] tokens = s.toCharArray();

        for (int i = 0; i < tokens.length; i++) {
            char c = tokens[i];
            if (c == ' ') continue;

            if (c == '(') {
                ops.push(c);
            } else if (c == ')') {
                while (ops.peek() != '(') {
                    vals.push(applyOp(ops.pop(), vals.pop(), vals.pop()));
                }
                ops.pop();
            } else if (c == '+' || c == '-' || c == '*' || c == '/') {
                while (!ops.isEmpty() && hasPrecedence(c, ops.peek())) {
                    vals.push(applyOp(ops.pop(), vals.pop(), vals.pop()));
                }
                ops.push(c);
            } else if (c >= '0' && c <= '9') {
                StringBuilder sb = new StringBuilder();
                while (i < tokens.length && tokens[i] >= '0' && tokens[i] <= '9') {
                    sb.append(tokens[i++]);
                }
                i--;
                vals.push(Integer.parseInt(sb.toString()));
            }
        }
        while (!ops.isEmpty()) {
            vals.push(applyOp(ops.pop(), vals.pop(), vals.pop()));
        }
        return vals.pop();
    }

    public boolean hasPrecedence(char o1, char o2) {
        if (o2 == '(' || o2 == ')') return false;
        if ((o1 =='*' || o1 == '/') && (o2 == '+' || o2 == '-')) {
            return false;
        } else {
            return true;
        }
    }

    public int applyOp(char o, int b, int a) {
        switch (o) {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '/':
                if (b == 0) throw new UnsupportedOperationException("cannot divide by zero");
                return a / b;
        }
        return 0;
    }
}