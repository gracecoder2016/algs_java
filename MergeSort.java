import java.util.*;
public class MergeSort {
    private static void merge (Comparable[] A, Comparable[] aux, int lo, int mid, int hi) {
        assert isSorted(A, lo, mid);
        assert isSorted(A, mid + 1, hi);

        for (int i = lo; i <= hi; i++) {
            aux[i] = A[i];
        }

        int i = lo, j = mid + 1;
        for (int k = lo; k <= hi; k++) {
            if (i > mid) {
                A[k] = aux[j++];
            } else if (j > hi) {
                A[k] = aux[i++];
            } else if (less(aux[j], aux[i])) {
                A[k] = aux[j++];
            } else {
                A[k] = aux[i++];
            }
        }
        assert isSorted(A, lo, hi);
    }

    public static void sort(Comparable[] A, Comparable[] aux, int lo, int hi) {
        if (hi <= lo) return;
        int mid = lo + (hi - lo) / 2;
        sort(A, aux, lo, mid);
        sort(A, aux, mid + 1, hi);
        merge(A, aux, lo, mid, hi);
    }

    public static void sort (Comparable[] A) {
        Comparable[] aux = new Comparable[A.length];
        sort(A, aux, 0, A.length - 1);
        assert isSorted(A);
    }

    public static void mergeSortBU(Comparable[] A) {
        int n = A.length;
        Comparable[] aux = new Comparable[n];
        for (int len = 1; len < n; len *= 2) {
            for (int lo = 0; lo < n - len; lo += len + len) {
                int mid = lo + len - 1;
                int hi = Math.min(lo + len + len - 1, n - 1);
                merge(A, aux, lo, mid, hi);
            }
        }
        assert isSorted(A);
    }

    private static boolean less (Comparable a, Comparable b) {
        return a.compareTo(b) < 0;
    }

    private static boolean less (Comparator comparator, Object a, Object b) {
        return comparator.compare(a, b) < 0;
    }

    private static boolean isSorted(Comparable[] A) {
        return isSorted(A, 0, A.length - 1);
    }
    private static boolean isSorted(Comparable[] A, int lo, int hi) {
        for (int i = lo + 1; i <= hi; i++) {
            if (less(A[i], A[i - 1])) {
                return false;
            }
        }
        return true;
    }

    public static void printArray(Comparable[] A) {
        for (int i = 0; i < A.length; i++) {
            System.out.println(A[i]);
        }
    }
}