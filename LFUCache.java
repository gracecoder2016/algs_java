class LFUCache {
    int capacity;
    int id;   // increment to define recentness
    Map<Integer, Integer> map;  // store original <Key, Value> pairs
    Map<Integer, Node> nodeMap;
    TreeSet<Node> tree;

    public LFUCache(int capacity) {
        this.capacity = capacity;
        this.id = 0;
        map = new HashMap<>();
        nodeMap = new HashMap<>();
        tree = new TreeSet<>();
    }

    public int get(int key) {
        id++;
        if (map.containsKey(key)) {
            update(key);
            return map.get(key);
        } else {
            return -1;  // throw exception
        }
    }

    public void put(int key, int value) {
        if (capacity == 0) return;
        id++;
        if (map.containsKey(key)) {
            update(key);
            map.put(key, value);
            return;
        }
        if (map.size() == capacity) {
            Node lfuNode = tree.pollFirst();
            map.remove(lfuNode.key);
            nodeMap.remove(lfuNode);
        }
        map.put(key, value);
        Node newNode = new Node(key, 1, id);
        nodeMap.put(key, newNode);
        tree.add(newNode);
    }

    private void update(int key) {
        Node old = nodeMap.get(key);
        int freq = old.f;
        tree.remove(old);
        Node newNode = new Node(key, freq + 1, id);
        nodeMap.put(key, newNode);
        tree.add(newNode);
    }
}
class Node implements Comparable<Node> {
    int key;
    int f;
    int r;
    public Node(int key, int f, int r) {
        this.key = key;
        this.f = f;
        this.r = r;
    }
    public boolean equals(Object object) {
        return key == ((Node) object).key;
    }
    public int hashCode(){
        return key;
    }
    public int compareTo(Node o) {
        return key == o.key? 0: (f == o.f ? (r - o.r ): (f - o.f));
    }
}

/**
 * Your LFUCache object will be instantiated and called as such:
 * LFUCache obj = new LFUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */