class Solution {
    public int[] pourWater(int[] H, int V, int K) {
        while (V-- > 0) {
            
            int i = K, best = K;
            while (0 <= i - 1 && H[i - 1] <= H[i]) {
                if (H[i - 1] < H[i]) best = i - 1;
                i--;
            }
            
            if (H[best] < H[K]) {
                H[best]++;
                continue;
            }
            
            while (i + 1 < H.length && H[i + 1] <= H[i]) {
                if (H[i + 1] < H[i]) best = i + 1;
                i++;
            }
            
            if (H[best] < H[K]) {
                H[best]++;
                continue;
            }
            H[K]++;
        }
        return H;
    }
}