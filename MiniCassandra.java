import java.util.*;
import java.util.TreeMap;

public class MiniCassandra {
    private Map<String, TreeMap<Integer, String>> map = new HashMap<>();
    public void insert(String raw_key, int col_key, String col_value) {
        if (!map.containsKey(raw_key)) {
            TreeMap<Integer, String> tm = new TreeMap<>();
            map.put(raw_key, tm);
        }
        map.get(raw_key).put(col_key, col_value);
    }

    public List<Column> query(String raw_key, int q_start, int q_end) {
        List<Column> rst = new ArrayList<>();
        TreeMap<Integer, String> tm = map.get(raw_key);
        if (tm == null) return rst;
        Map<Integer, String> queried = tm.subMap(q_start, true, q_end, true);
        for (Map.Entry<Integer, String> entry: queried.entrySet()) {
            rst.add(new Column(entry.getKey(), entry.getValue()));
        }
        return rst;
    }
    public static void main (String[] args) {
        MiniCassandra obj = new MiniCassandra();
        obj.insert("alice", 1, "content1");
        obj.insert("alice", 2, "content2");
        obj.insert("bob", 1, "post1");
        System.out.println(obj.query("alice", 0, 2).toString());
        System.out.println(obj.query("bob", 0, 2).toString());
    }
}

class Column {
    public int key;
    public String value;
    public Column(int key, String value) {
        this.key = key;
        this.value = value;
    }
    public String toString() {
        return "(" + key + ", " + value + ")";
    }
}
