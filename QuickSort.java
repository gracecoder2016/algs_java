public class QuickSort {
    public static void quicksort(int[] A, int start, int end) {
        if (start >= end) return;
        int left = start, right = end;
        int pivot = A[(start + end) /2];

        while (left <= right) {
            while (left <= right && A[left] < pivot) {
                left++;
            }
            while (left <= right && A[right] > pivot) {
                right--;
            }
            if (left <= right) {
                int temp = A[left];
                A[left] = A[right];
                A[right] = temp;

                left++;
                right--;
            }
        }
        quicksort(A, start, right);
        quicksort(A, left, end);
    }
}