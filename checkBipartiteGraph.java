// Java code to check if a strongly connected graph is a bipartite graph
import java.util.*;
import java.util.Queue;
public class Solution {
    final static int V = 4;
    public boolean isBipartite_StrongConnected(int[][] G, int src) {
        int[] color = new int[V];
        Arrays.fill(color, -1);
        color[src] = 1;

        // BFS
        Queue<Integer> q = new LinkedList<>();
        q.add(src);
        while (q.size() != 0) {
            int u = q.poll();
            if (G[u][u] == 1) return false; //self loop

            for (int v = 0; v < V; v++) {
                if (G[u][v] == 1 && color[v] == -1) {
                    color[v] = 1 - color[u];
                    q.offer(v);
                } else if (G[u][v] == 1 && color[v] == color[u]) {
                    return false;
                }
            }
        }
        return true;
    }
    public static void main (String[] args) {
        Solution sln = new Solution();
        int[][] G = {
                {0, 1, 0, 1},
                {1, 0, 1, 0},
                {0, 1, 0, 1},
                {1, 0, 1, 0}
        };
        System.out.println(sln.isBipartite_StrongConnected(G, 0));
    }
}

// This version works for not strongly connected graph. Repeatedly call chekcBipartite
// for all not yet visited vertices
import java.util.*;
import java.util.Queue;
public class Solution {
    final static int V = 4;
    public boolean isBipartite(int[][] G) {
        int[] color = new int[V];
        Arrays.fill(color, -1);
        for (int i = 0; i < V; i++) {
            if (color[i] == -1) {
                if (!isBipartiteSub(G, i, color)) return false;
            }
        }
        return true;
    }

    public boolean isBipartiteSub(int[][] G, int src, int[] color) {
        color[src] = 1;
        // BFS
        Queue<Integer> q = new LinkedList<>();
        q.add(src);
        while (q.size() != 0) {
            int u = q.poll();
            if (G[u][u] == 1) return false; //self loop

            for (int v = 0; v < V; v++) {
                if (G[u][v] == 1 && color[v] == -1) {
                    color[v] = 1 - color[u];
                    q.offer(v);
                } else if (G[u][v] == 1 && color[v] == color[u]) {
                    return false;
                }
            }
        }
        return true;
    }
    public static void main (String[] args) {
        Solution sln = new Solution();
        int[][] G = {
                {0, 1, 0, 1},
                {1, 0, 1, 0},
                {0, 1, 0, 1},
                {1, 0, 1, 0}
        };
        System.out.println(sln.isBipartite(G));
    }
}

